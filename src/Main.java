import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        Column column1 = new Column(1);
        Column column2 = new Column(2);
        Column column3 = new Column(3);
        column1.addElements(10);

        column1.printColumn();
        column2.printColumn();
        column3.printColumn();

        boolean play = true;
        while (play) {
            System.out.println("Input columns divided by /; enter q to quit");
            String command = scanner.nextLine();
            String splitComm [] = command.split("/");
            switch (splitComm[0]) {
                case "1":
                    switch (splitComm[1]) {
                        case "2":
                            column1.moveElement(column2);
                            break;
                        case "3":
                            column1.moveElement(column3);
                            break;
                        default:
                                continue;
                    }
                    break;
                case "2":
                    switch (splitComm[1]) {
                        case "1":
                            column2.moveElement(column1);
                            break;
                        case "3":
                            column2.moveElement(column3);
                            break;
                        default:
                            continue;
                    }
                    break;
                case "3":
                    switch (splitComm[1]) {
                        case "1":
                            column3.moveElement(column1);
                            break;
                        case "2":
                            column3.moveElement(column2);
                            break;
                        default:
                                continue;
                    }
                case "q":
                    play = false;
                    break;
            }
                    column1.printColumn();
                    column2.printColumn();
                    column3.printColumn();
            }

        }
    }