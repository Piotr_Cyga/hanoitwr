import java.util.List;
import java.util.Stack;

public class Column {
    private Stack<Integer> stack = new Stack<>();
    private int nr;

    public Column(int nr) {
        this.nr = nr;
    }

    public List<Integer> getStack() {
        return stack;
    }

    public void setStack(Stack<Integer> stack) {
        this.stack = stack;
    }

    public void addElements(int elemNr) {
        for (Integer i = 0; i < elemNr; i++) {
            stack.add(i + 1);
        }
    }

    public void moveElement(Column to) {
        Integer disk = stack.pop();
        if (!to.stack.empty()) {Integer diskBelow = to.stack.peek();
            if (diskBelow > disk) {
            to.stack.push(disk);
            }
            else stack.push(disk);
        }
        else to.stack.push(disk);
    }
    public void printColumn()
    {
        System.out.print("column " + nr + ": ");
        for (Integer value:stack)
        {
            System.out.print(value + ", ");
        }
        System.out.println();
    }
}